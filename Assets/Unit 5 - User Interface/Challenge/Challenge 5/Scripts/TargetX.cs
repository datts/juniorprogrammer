﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Unit5UserInterface
{
    public class TargetX : MonoBehaviour
    {
        public int pointValue;
        public GameObject explosionFx;

        private float timeOnScreen = 1.0f;

        private const float MIN_VALUE_X = -3.75f; // the x value of the center of the left-most square
        private const float MIN_VALUE_Y = -3.75f; // the y value of the center of the bottom-most square
        private float spaceBetweenSquares = 2.5f; // the distance between the centers of squares on the game board


        void Start()
        {
            transform.position = RandomSpawnPosition();
            StartCoroutine(RemoveObjectRoutine()); // begin timer before target leaves screen

        }

        // When target is clicked, destroy it, update score, and generate explosion
        private void OnMouseDown()
        {
            if (GameManagerX.Instance.isGameActive)
            {
                Destroy(gameObject);
                GameManagerX.Instance.UpdateScore(pointValue);
                Explode();
            }

        }

        // Generate a random spawn position based on a random index from 0 to 3
        Vector3 RandomSpawnPosition()
        {
            float spawnPosX = MIN_VALUE_X + (RandomSquareIndex() * spaceBetweenSquares);
            float spawnPosY = MIN_VALUE_Y + (RandomSquareIndex() * spaceBetweenSquares);

            Vector3 spawnPosition = new Vector3(spawnPosX, spawnPosY, 0);
            return spawnPosition;

        }

        // Generates random square index from 0 to 3, which determines which square the target will appear in
        int RandomSquareIndex()
        {
            return Random.Range(0, 4);
        }


        // If target that is NOT the bad object collides with sensor, trigger game over
        private void OnTriggerEnter(Collider other)
        {
            Destroy(gameObject);

            if (other.gameObject.CompareTag("Sensor") && !gameObject.CompareTag("Bad"))
            {
                GameManagerX.Instance.GameOver();
            }

        }

        // Display explosion particle at object's position
        void Explode()
        {
            Instantiate(explosionFx, transform.position, explosionFx.transform.rotation);
        }

        // After a delay, Moves the object behind background so it collides with the Sensor object
        IEnumerator RemoveObjectRoutine()
        {
            yield return new WaitForSeconds(timeOnScreen);
            if (GameManagerX.Instance.isGameActive)
            {
                transform.Translate(Vector3.forward * 5, Space.World);
            }

        }
    }

}

