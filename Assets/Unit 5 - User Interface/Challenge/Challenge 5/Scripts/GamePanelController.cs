using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Unit5UserInterface
{
    public class GamePanelController : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI gameOverText;
        [SerializeField] private GameObject gameOverUI;
        [SerializeField] private Button restartButton;

        public void ActiveGameOver(bool isActive)
        {
            gameOverUI.gameObject.SetActive(isActive);
        }
        public void GameOver()
        {
            gameOverText.gameObject.SetActive(true);
            restartButton.gameObject.SetActive(true);
        }
    }
}
