﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Unit5UserInterface
{
    public class GameManagerX : MonoBehaviour
    {
        public static GameManagerX Instance { get; private set; }
        private void Awake()
        {
            // If there is an instance, and it's not me, delete myself.

            if (Instance != null && Instance != this)
            {
                Destroy(this);
            }
            else
            {
                Instance = this;
            }
        }

        [SerializeField] private TextMeshProUGUI scoreText;
        [SerializeField] private TextMeshProUGUI timePlay;

        public List<GameObject> targetPrefabs;

        [SerializeField] private GamePanelController gamePanelController;

        [SerializeField] private SpawnManager spawnManager;
        private int score;
        private float spawnRate = 1.5f;
        public bool isGameActive;

        private int timePlayNumber = 60;

        // Start the game, remove title screen, reset score, and adjust spawnRate based on difficulty button clicked
        public void StartGame(int difficulty)
        {
            spawnRate /= difficulty;
            isGameActive = true;
            StartCoroutine(timePlayToEndGame());
            if (spawnManager != null)
            {
                spawnManager.StartSpawn(spawnRate);
            }
            score = 0;
            UpdateScore(0);
            gamePanelController.ActiveGameOver(false);

        }

        // Update score with value from target clicked
        public void UpdateScore(int scoreToAdd)
        {
            score += scoreToAdd;
            scoreText.text = "score: " + score;
        }

        public void UpdateTime(int time)
        {
            timePlay.text = "Time: " + time.ToString();
        }

        // Stop game, bring up game over text and restart button
        public void GameOver()
        {
            gamePanelController.GameOver();
            isGameActive = false;
        }

        // Restart game by reloading the scene
        public void RestartGame()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
        IEnumerator timePlayToEndGame()
        {
            while (timePlayNumber > 0 && isGameActive == true)
            {
                timePlay.text = "Time: " + timePlayNumber.ToString();
                yield return new WaitForSeconds(1f);
                timePlayNumber--;
                if (timePlayNumber <= 0)
                {
                    UpdateTime(timePlayNumber);
                    GameOver();

                }
            }
        }
    }
}
