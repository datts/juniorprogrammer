using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Unit5UserInterface
{
    public class SpawnManager : MonoBehaviour
    {
        private float spaceBetweenSquares = 2.5f;
        private float minValueX = -3.75f; //  x value of the center of the left-most square
        private float minValueY = -3.75f; //  y value of the center of the bottom-most square


        public List<GameObject> targetPrefabs;
        // Start is called before the first frame update
        IEnumerator SpawnTarget(float spawnRate)
        {
            while (GameManagerX.Instance.isGameActive)
            {
                yield return new WaitForSeconds(spawnRate);
                int index = Random.Range(0, targetPrefabs.Count);

                if (GameManagerX.Instance.isGameActive)
                {
                    Instantiate(targetPrefabs[index], RandomSpawnPosition(), targetPrefabs[index].transform.rotation);
                }

            }
        }
        public void StartSpawn(float spawnRate)
        {
            StartCoroutine(SpawnTarget(spawnRate));
        }
        Vector3 RandomSpawnPosition()
        {
            float spawnPosX = minValueX + (RandomSquareIndex() * spaceBetweenSquares);
            float spawnPosY = minValueY + (RandomSquareIndex() * spaceBetweenSquares);

            Vector3 spawnPosition = new Vector3(spawnPosX, spawnPosY, 0);
            return spawnPosition;

        }
        int RandomSquareIndex()
        {
            return Random.Range(0, 4);
        }
    }
}

