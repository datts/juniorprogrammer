﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Unit5UserInterface
{
    public class DifficultyButtonX : MonoBehaviour
    {
        private Button button;
        public int difficulty;

        // Start is called before the first frame update
        void Start()
        {
            button = GetComponent<Button>();
            button.onClick.AddListener(SetDifficulty);
        }

        /* When a button is clicked, call the StartGame() method
         * and pass it the difficulty value (1, 2, 3) from the button 
        */
        void SetDifficulty()
        {
            GameManagerX.Instance.StartGame(difficulty);
        }



    }
}

