using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Unit5UserInterface
{
    public class DifficultyButton : MonoBehaviour
    {
        private Button button;
        [SerializeField ]private GameManager gameManager;
        public int difficulty;
        private void Start()
        {
            button = GetComponent<Button>();
            button.onClick.AddListener(SetDifficulty);
            gameManager = GameObject.FindObjectOfType<GameManager>();
            
        }
        void SetDifficulty()
        {
            gameManager.StartGame(difficulty);
        }
    }
}
