using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Unit5UserInterface
{
    public class Target : MonoBehaviour
    {
        private Rigidbody targetRb;
        private float minSpeed = 12;
        private float maxSpeed = 16;
        private float maxTorque = 10;
        private float xRange = 4;
        private float ySpawnPos = -6;

        private GameManager gameManager;
        [SerializeField] private int pointValue = 5;
        [SerializeField] private ParticleSystem explosionParticle;


        // Start is called before the first frame update
        void Start()
        {
            targetRb = GetComponent<Rigidbody>();
            targetRb.AddForce(RandomForce(), ForceMode.Impulse);
            targetRb.AddTorque(RandomTorque(), RandomTorque(), RandomTorque(), ForceMode.Impulse);
            transform.position = RandomSpawnPos();

            gameManager = GameObject.FindObjectOfType<GameManager>();
        }
        private void Update()
        {
            if (gameManager.isGameActive == false)
            {
                Destroy(gameObject);
            }
        }

        Vector3 RandomForce()
        {
            return Vector3.up * Random.Range(minSpeed, maxSpeed);
        }
        float RandomTorque()
        {
            return Random.Range(-maxTorque, maxTorque);
        }
        Vector3 RandomSpawnPos()
        {
            return new Vector3(Random.Range(-xRange, xRange), ySpawnPos);
        }
        private void OnMouseDown()
        {
            if (gameManager.isGameActive)
            {
                Destroy(gameObject);
                Instantiate(explosionParticle, transform.position, explosionParticle.transform.rotation);
                gameManager.UpdateScore(pointValue);
            }

        }
        private void OnTriggerEnter(Collider other)
        {
            if (!gameObject.CompareTag("Bad") && other.CompareTag("Sensor"))
            {
                gameManager.GameOver();
            }
        }
    }
}
