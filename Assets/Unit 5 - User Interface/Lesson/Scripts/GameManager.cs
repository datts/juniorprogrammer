using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Unit5UserInterface
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private PanelUIController panelUI;
        [SerializeField] private List<GameObject> targets;
        private float spawnRate = 1.0f;

        private bool isPause;
        private int score;

        public bool isGameActive;

        public void StartGame(int difficulty)
        {
            spawnRate /= difficulty;
            isGameActive = true;
            score = 0;

            StartCoroutine(SpawnTarget());
            panelUI.ActiveStartPanel(false);
            panelUI.ActiveScoreText(true);

            isPause = false;
            panelUI.ActivePauseGame(true);

        }

        IEnumerator SpawnTarget()
        {
            while (isGameActive)
            {
                yield return new WaitForSeconds(spawnRate);
                int index = Random.Range(0, targets.Count);
                Instantiate(targets[index]);
            }
        }
        public void UpdateScore(int scoreToAdd)
        {
            score += scoreToAdd;
            panelUI.SetTextScoreText(score);
        }
        public void GameOver()
        {
            isGameActive = false;
            panelUI.ActiveGameOver(true);
            panelUI.ActiveRestartButton(true);
            panelUI.ActivePauseGame(false);
        }

        public void RestartGame()
        {
            //SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            panelUI.ActiveStartPanel(true);
            panelUI.ActiveGameOver(false);
            panelUI.ActiveRestartButton(false);
            panelUI.ActiveScoreText(false);
        }

        public void PauseGame()
        {

            if (isPause == false)
            {
                Time.timeScale = 0;
                isPause = true;
            }
            else
            {
                Time.timeScale = 1;
            }
        }
    }
}
