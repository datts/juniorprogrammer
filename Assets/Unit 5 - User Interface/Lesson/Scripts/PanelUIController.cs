using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class PanelUIController : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI scoreText;
    [SerializeField] private GameObject gameOverText;
    [SerializeField] private Button restartButton;
    [SerializeField] private GameObject startPanel;
    [SerializeField] private Button pauseGame;

    public void ActiveScoreText(bool isActive)
    {
        scoreText.gameObject.SetActive(isActive);
    }
    public void SetTextScoreText(int score)
    {
        scoreText.text = "Score: " + score;
    }
    public void ActiveStartPanel(bool isActive)
    {
        startPanel.gameObject.SetActive(isActive);
    }
    public void ActivePauseGame(bool isActive)
    {
        pauseGame.gameObject.SetActive(isActive);
    }
    public void ActiveGameOver(bool isActive)
    {
        gameOverText.gameObject.SetActive(isActive);
    }
    public void ActiveRestartButton(bool isActive)
    {
        restartButton.gameObject.SetActive(isActive);
    }

}
