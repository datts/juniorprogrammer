﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayerX : MonoBehaviour
{
    public GameObject plane;
    private Vector3 offset;

    // Start is called before the first frame update
    void Start()
    {
        offset = new Vector3(-16.5f, 1, 00);
        gameObject.transform.eulerAngles = new Vector3(0, 90, 0);
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            offset = new Vector3(0, 4, -6);
            gameObject.transform.eulerAngles = new Vector3(30, 0, 0);
        }
        if (Input.GetKeyDown(KeyCode.G))
        {
            offset = new Vector3(-16.5f, 1, 00);
            gameObject.transform.eulerAngles = new Vector3(0, 90, 0);
        }
        transform.position = plane.transform.position + offset;

    }
}
