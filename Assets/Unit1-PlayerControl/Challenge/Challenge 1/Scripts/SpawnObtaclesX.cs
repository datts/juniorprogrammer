using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnObtaclesX : MonoBehaviour
{
    [SerializeField] private GameObject obtacles;
    [SerializeField] private GameObject player;

    [SerializeField] private List<GameObject> newRoadSpawnList;
    private Vector3 positionRoad;
    // Start is called before the first frame update
    void Start()
    {
        SpawnRandom();
    }

    // Update is called once per frame
    void Update()
    {
        if (newRoadSpawnList[newRoadSpawnList.Count-1].transform.position.z - player.transform.position.z < 1)
        {
            SpawnRandom();
        }
    }
    public void SpawnRandom()
    { 
        GameObject newRoad = Instantiate(obtacles, obtacles.transform.position, Quaternion.identity);
        positionRoad += new Vector3(0, Random.Range(-20, 20), Random.Range(20, 40)); ;
        newRoad.transform.position = positionRoad;
        newRoadSpawnList.Add(newRoad);
    }
}
