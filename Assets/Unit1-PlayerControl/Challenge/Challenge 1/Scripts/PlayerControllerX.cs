﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerX : MonoBehaviour
{
    [SerializeField] private float speed = 20f;
    [SerializeField] private float rotationSpeed = 50;
    [SerializeField] private float horizontalInput;
    [SerializeField] private float verticalInput;
    private float balance = 0f;

    private const float BALANCE_AFTER_TIME = 0.5f;
    private const float TIME_BALANCE = 0.15f;

    private Quaternion originalRotationValue;
    // Update is called once per frame
    private void Start()
    {
        originalRotationValue = transform.rotation; // save the initial rotation
    }
    void FixedUpdate()
    {
        // get the user's vertical input
        //This is where we get player input
        horizontalInput = Input.GetAxis("Horizontal");
        verticalInput = Input.GetAxis("Vertical");
        // move the plane forward at a constant rate
        transform.Translate(Vector3.forward * speed * Time.deltaTime);

        transform.Rotate(Vector3.left, rotationSpeed * verticalInput * Time.deltaTime);
        transform.Rotate(Vector3.forward, rotationSpeed * horizontalInput * Time.deltaTime);

        balance += Time.deltaTime;
        if (balance > BALANCE_AFTER_TIME)
        {
            if (horizontalInput == 0 && verticalInput == 0)
            {
                //transform.rotation = Quaternion.identity.normalized;
                transform.rotation = Quaternion.Slerp(transform.rotation, originalRotationValue, TIME_BALANCE);

            }
            if (transform.eulerAngles == Vector3.zero)
            {
                balance = 0;
            }
        }

    }
    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log("Gameover!");
        Time.timeScale = 0;
    }
}
