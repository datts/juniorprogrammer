using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnRoad : MonoBehaviour
{
    [SerializeField] private GameObject road;
    [SerializeField] private GameObject player;

    private Vector3 distanceRoad = new Vector3(-70, 0, 0);
    private Vector3 positionRoad;


    [SerializeField] private List<GameObject> newRoadSpawnList;
    // Start is called before the first frame update
    void Start()
    {
        SpawnRandom();
    }

    // Update is called once per frame
    void Update()
    {
        if (Math.Abs(Vector3.Distance(newRoadSpawnList[newRoadSpawnList.Count - 1].transform.position, player.transform.position)) < 10)
        {
            Debug.Log("position" + newRoadSpawnList[newRoadSpawnList.Count - 1].transform.position);
            NextSpawn();
        }

    }
    public void SpawnRandom()
    {
        var firstRoad = Instantiate(road, road.transform.position, road.transform.rotation);
        positionRoad = firstRoad.transform.position;
        newRoadSpawnList.Add(firstRoad);
    }
    public void NextSpawn()
    {
        GameObject newRoad = Instantiate(road, road.transform.position, road.transform.rotation) as GameObject;
        positionRoad += distanceRoad;
        newRoad.transform.position = positionRoad;
        newRoadSpawnList.Add(newRoad);
    }
}
