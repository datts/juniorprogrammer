using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Unit1PlayerControll
{
    public class PlayerController : MonoBehaviour
    {
        public float speed = 20f;
        public float turnSpeed = 45.0f;
        public float horizontalInput;
        public float verticalInput;
        // Start is called before the first frame update

        // Update is called once per frame
        void Update()
        {
            //This is where we get player input
            horizontalInput = Input.GetAxis("Horizontal");
            verticalInput = Input.GetAxis("Vertical");

            //We move the vehicle forward
            transform.Translate(Vector3.forward * Time.deltaTime * verticalInput * speed);
            transform.Rotate(Vector3.up, turnSpeed * horizontalInput * Time.deltaTime);

        }
        private void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.CompareTag("Obstacles"))
            {
                Debug.Log("Gameover!");
                Time.timeScale = 0;
            }
        }
    }
}
