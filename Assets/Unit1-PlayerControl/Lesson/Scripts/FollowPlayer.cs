using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Unit1PlayerControll
{
    public class FollowPlayer : MonoBehaviour
    {
        [SerializeField] private Transform cameraTarget;
        [SerializeField] private Vector3 dist;
        [SerializeField] private float speed = 10.0f;
        [SerializeField] private Transform lookTarget;

        //Update is called once per frame
        void Update()
        {
            Vector3 dPos = cameraTarget.position + dist;
            Vector3 sPos = Vector3.Lerp(transform.position, dPos, speed * Time.deltaTime);
            transform.position = sPos;
            transform.LookAt(lookTarget.position);
        }

    }
}