using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace Unit1PlayerControll
{
    public class SpawnObtacles : MonoBehaviour
    {
        public GameObject[] obtacles;
        public GameObject spawnPoint;
        private void Start()
        {
            SpawnRandomObtaclesFollowX();
        }
        private void SpawnRandomObtaclesFollowX()
        {
            for (int i = 0; i < 5; i++)
            {
                SpawnRandomObtaclesFollowZ();
                spawnPoint.transform.position = new Vector3(spawnPoint.transform.position.x - 15, spawnPoint.transform.position.y, spawnPoint.transform.position.z);

            }
        }
        private void SpawnRandomObtaclesFollowZ()
        {
            int beforeZ = 0;
            for (int i = 0; i < 2; i++)
            {
                int posZ = Random.Range(-4, 4) * 2; //Obstacle is not touching if random to near

                if (beforeZ != posZ || beforeZ == 0)
                {
                    beforeZ = posZ;

                    spawnPoint.transform.position = new Vector3(spawnPoint.transform.position.x, spawnPoint.transform.position.y, posZ);
                    Instantiate(obtacles[Random.Range(0, obtacles.Length - 1)], spawnPoint.transform.position, Quaternion.identity);
                }
            }
        }
    }
}
