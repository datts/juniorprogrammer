﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cube : MonoBehaviour
{
    public MeshRenderer Renderer;
    private float timeChangeColor = 0;
    void Start()
    {
        transform.position = new Vector3(0, 0, 0);
        transform.localScale = Vector3.one * 1.3f;

        Material material = Renderer.material;
        material.color = new Color(2.5f, 2.0f, 0.3f, 0.4f);
    }

    void Update()
    {
        Material material = Renderer.material;
        timeChangeColor += Time.deltaTime;
        if (timeChangeColor > 2)
        {
            material.color = new Color(Random.Range(0f, 3f), Random.Range(0f, 3f), Random.Range(0f, 1f), 0.4f);
            
            timeChangeColor = 0;
        }
        transform.Rotate(1.0f * (Time.deltaTime), 0.5f, 0.5f);

    }
}
