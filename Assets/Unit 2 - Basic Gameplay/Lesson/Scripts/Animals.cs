using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Unit2BasicGameplay
{
    public class Animals : MonoBehaviour
    {
        [SerializeField] private float speed = 40f;
        private float lowerBound = -10f;
        void Update()
        {
            if (transform.position.z < lowerBound)
            {
                Debug.Log("Game Over!");
                Destroy(gameObject);
            }
            transform.Translate(Vector3.forward * Time.deltaTime * speed);
        }
    }
}
