using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Unit2BasicGameplay
{
    public class SpawnManager : MonoBehaviour
    {
        [SerializeField] private GameObject[] animalPrefabs;
        [SerializeField] private float spawnRangeX = 20;
        [SerializeField] private float spawnPosZ = 20;

        [SerializeField] private float startDelay = 2f;
        [SerializeField] private float spawnInterval = 1.5f;

        // Start is called before the first frame update
        void Start()
        {
            InvokeRepeating("SpawnRandomAnimal", startDelay, spawnInterval);
        }

        void SpawnRandomAnimal()
        {
            int animalIndex = Random.Range(0, animalPrefabs.Length);
            Vector3 spawnPos = new Vector3(Random.Range(-spawnRangeX, spawnRangeX), 0, spawnPosZ);
            Instantiate(animalPrefabs[animalIndex], spawnPos, animalPrefabs[animalIndex].transform.rotation);
        }
    }
}