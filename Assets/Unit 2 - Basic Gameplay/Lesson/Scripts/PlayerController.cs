using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Unit2BasicGameplay
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private float horizontalInput;
        [SerializeField] private float speed = 10.0f;
        [SerializeField] private float xRange = 20; //same rang cam
        [SerializeField] private GameObject projectilePrefab;

        // Update is called once per frame
        void Update()
        {
            LimitMove();
            horizontalInput = Input.GetAxis("Horizontal");
            transform.Translate(Vector3.right * horizontalInput * Time.deltaTime * speed);
            Shoot();
        }
        void LimitMove()
        {
            if (transform.position.x < -xRange)
            {
                transform.position = new Vector3(-xRange, transform.position.y, transform.position.z);
            }
            if (transform.position.x > xRange)
            {
                transform.position = new Vector3(xRange, transform.position.y, transform.position.z);
            }
        }
        void Shoot()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                //Launch a project title from player
                Instantiate(projectilePrefab, transform.position + new Vector3(0, 1, 0), projectilePrefab.transform.rotation);
            }
        }
    }
}