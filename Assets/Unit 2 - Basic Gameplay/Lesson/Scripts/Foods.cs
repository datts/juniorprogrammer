using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Unit2BasicGameplay
{
    public class Foods : MonoBehaviour
    {
        [SerializeField] private float speed = 40f;
        // Update is called once per frame
        private float topBound = 30f;
        void Update()
        {
            transform.Translate(Vector3.forward * Time.deltaTime * speed);
            if (transform.position.z > topBound)
            {
                Destroy(gameObject);
            }
        }
        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Enemy"))
            {
                Destroy(other.gameObject);
            }
        }
    }
}
