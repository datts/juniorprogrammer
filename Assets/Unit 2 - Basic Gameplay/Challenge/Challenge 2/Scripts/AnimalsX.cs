using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Unit2BasicGameplay
{
    public class AnimalsX : MonoBehaviour
    {
        private float leftLimit = 40;
        private float speed = 30;
        // Update is called once per frame
        void Update()
        {
            transform.Translate(Vector3.forward * speed * Time.deltaTime);
            // Destroy dogs if x position less than left limit
            if (transform.position.x > leftLimit)
            {
                Destroy(gameObject);
            }
            if (transform.position.x < -leftLimit)
            {
                Destroy(gameObject);
            }
        }
        private void OnTriggerEnter(Collider other)
        {
            Destroy(gameObject);
        }
    }
}
