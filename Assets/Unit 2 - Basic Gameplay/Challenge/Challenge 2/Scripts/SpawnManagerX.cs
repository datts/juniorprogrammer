﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Unit2BasicGameplay
{
    public class SpawnManagerX : MonoBehaviour
    {
        public GameObject[] ballPrefabs;

        private float spawnLimitXLeft = -22;
        private float spawnLimitXRight = 7;
        private float spawnPosY = 30;


        // Start is called before the first frame update
        void Start()
        {
            StartCoroutine(SpawnRandomBallIE());
        }

        // Spawn random ball at random x position at top of play area
        void SpawnRandomBall()
        {
            // Generate random ball index and random spawn position
            Vector3 spawnPos = new Vector3(Random.Range(spawnLimitXLeft, spawnLimitXRight), spawnPosY, 0);

            // instantiate ball at random spawn location
            int indexBall = Random.Range(0, ballPrefabs.Length);
            Instantiate(ballPrefabs[indexBall], spawnPos, ballPrefabs[indexBall].transform.rotation);
        }
        IEnumerator SpawnRandomBallIE()
        {
            while (true)
            {
                yield return new WaitForSeconds(Random.Range(1, 5));
                SpawnRandomBall();

            }
        }

    }
}