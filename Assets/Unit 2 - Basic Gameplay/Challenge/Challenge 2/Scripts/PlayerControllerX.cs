﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Unit2BasicGameplay
{
    public class PlayerControllerX : MonoBehaviour
    {
        public GameObject dogPrefab;
        private float timeSpace = 0;
        private float amoutTime = 2;

        // Update is called once per frame
        void Update()
        {
            timeSpace += Time.deltaTime;
            // On spacebar press, send dog
            if (Input.GetKeyDown(KeyCode.Space))
            {
                if (timeSpace >= amoutTime)
                {
                    Instantiate(dogPrefab, transform.position, dogPrefab.transform.rotation);
                    timeSpace = 0;
                }
            }


        }
    }
}