using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Unit2BasicGameplay
{
    public class BallsX : MonoBehaviour
    {
        private float bottomLimit = -10;
        private float speed = 1;
        // Update is called once per frame
        void Update()
        {
            transform.Translate(Vector3.forward * speed * Time.deltaTime);
            if (transform.position.y < bottomLimit)
            {
                Destroy(gameObject);
                Debug.Log("GAME OVER");
            }
        }
        private void OnTriggerEnter(Collider other)
        {
            Destroy(gameObject);
        }
    }
}
