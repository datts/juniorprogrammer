using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Unit4GameplayMechanics
{
    public class SpawnManager : MonoBehaviour
    {
        [SerializeField] private GameObject enemyPrefabs;
        [SerializeField] private GameObject powerupPrefab;
        [SerializeField] private GameObject shootPrefab;
        [SerializeField] private float spawnRange = 9;
        [SerializeField] private int enemyCount;
        public int waveNumber = 1;
        // Start is called before the first frame update
        void Start()
        {
            Instantiate(powerupPrefab, GenerateSpawnPosition(), powerupPrefab.transform.rotation);
            Instantiate(shootPrefab, GenerateSpawnPosition(), shootPrefab.transform.rotation);
            SpawnEnemyWave(waveNumber);
        }

        // Update is called once per frame
        void Update()
        {
            enemyCount = FindObjectsOfType<Enemy>().Length;

            if (enemyCount == 0)
            {
                waveNumber++;
                SpawnEnemyWave(waveNumber);
                Instantiate(powerupPrefab, GenerateSpawnPosition(), powerupPrefab.transform.rotation);
                Instantiate(shootPrefab, GenerateSpawnPosition(), shootPrefab.transform.rotation);
            }
        }
        private Vector3 GenerateSpawnPosition()
        {
            float spawnPosX = Random.RandomRange(-spawnRange, spawnRange);
            float spawnPosZ = Random.RandomRange(-spawnRange, spawnRange);
            return new Vector3(spawnPosX, 0, spawnPosZ);
        }
        private void SpawnEnemyWave(int enemiesToSpawn)
        {
            for (int i = 0; i < enemiesToSpawn; i++)
            {
                Instantiate(enemyPrefabs, GenerateSpawnPosition(), enemyPrefabs.transform.rotation);
            }
        }
    }
}
