using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Unit4GameplayMechanics
{
    public class Projectile : MonoBehaviour
    {
        [SerializeField] private float speed = 20f;
        private void Update()
        {
            transform.Translate(Vector3.forward * Time.deltaTime * speed);
            Destroy(gameObject, 2);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Enemy"))
            {
                Destroy(other.gameObject);
            }
        }
    }
}
