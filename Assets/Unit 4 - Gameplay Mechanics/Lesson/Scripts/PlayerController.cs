using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Unit4GameplayMechanics
{
    public class PlayerController : MonoBehaviour
    {
        private const int POSITION_DESTROY_OY = -5;
        private Rigidbody playerRb;
        [SerializeField] private float speed = 5.0f;
        private RotateCamera focalPoint;
        [SerializeField] private bool hasPowerup;
        [SerializeField] private bool hasShootBoost;
        [SerializeField] private float powerupStrength = 150.0f;

        [SerializeField] private GameObject powerupIndicator;
        [SerializeField] private GameObject shootIndicator;

        [SerializeField] private ShootingController shootingController;

        // Start is called before the first frame update
        void Start()
        {
            playerRb = GetComponent<Rigidbody>();
            focalPoint = GameObject.FindObjectOfType<RotateCamera>();
        }

        // Update is called once per frame
        void Update()
        {
            TransfomPlayer();
            CheckPositionInIsland();
            Shoot();

        }

        private void TransfomPlayer()
        {
            float forwardInput = Input.GetAxis("Vertical");
            float horizontalInput = Input.GetAxis("Horizontal");

            playerRb.AddForce(focalPoint.transform.forward * speed * forwardInput);
            playerRb.transform.eulerAngles += (Vector3.up * horizontalInput);
            shootIndicator.transform.eulerAngles = Vector3.zero;
            powerupIndicator.transform.eulerAngles = Vector3.zero;
        }
        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("PowerUp"))
            {
                hasPowerup = true;
                Destroy(other.gameObject);
                StartCoroutine(PowerupCountdownRoutine());
                powerupIndicator.gameObject.SetActive(true);
            }
            else if (other.CompareTag("ShootBooster"))
            {
                hasShootBoost = true;
                Destroy(other.gameObject);
                StartCoroutine(ShootBoostCountdownRoutine());
                shootIndicator.gameObject.SetActive(true);
            }
        }
        private void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.CompareTag("Enemy") && hasPowerup)
            {
                if (hasPowerup)
                {
                    Rigidbody enemyRigidbody = collision.gameObject.GetComponent<Rigidbody>();
                    Vector3 awayFromPlayer = (collision.gameObject.transform.position - transform.position);
                    enemyRigidbody.AddForce(awayFromPlayer * powerupStrength, ForceMode.Impulse);
                }
            }
        }

        private IEnumerator PowerupCountdownRoutine()
        {
            yield return new WaitForSeconds(7);
            hasPowerup = false;
            powerupIndicator.gameObject.SetActive(false);
        }
        private IEnumerator ShootBoostCountdownRoutine()
        {
            
            yield return new WaitForSeconds(5);
            hasShootBoost = false;
            shootIndicator.gameObject.SetActive(false);
        }
        private void Shoot()
        {
            if (Input.GetKeyDown(KeyCode.Space) && hasShootBoost)
            {
                shootingController.Shoot();
            }
        }

        private void CheckPositionInIsland()
        {
            if (transform.position.y < POSITION_DESTROY_OY)
            {
                transform.position = Vector3.zero;
                playerRb.velocity = Vector3.zero;
                playerRb.angularVelocity = Vector3.zero;
            }
        }

    }
}
