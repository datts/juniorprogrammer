using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingController : MonoBehaviour
{
    [SerializeField] private GameObject bulletPrefabs;
    // Update is called once per frame
    public void Shoot()
    {
        Instantiate(bulletPrefabs, transform.position, transform.rotation);
    }
}
