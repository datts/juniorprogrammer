using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Unit4GameplayMechanics
{
    public class Enemy : MonoBehaviour
    {
        private const int POSITION_DESTROY_OY = -10;
        [SerializeField] private float speed;
        private Rigidbody enemyRb;
        [SerializeField] private PlayerController player;

        // Start is called before the first frame update
        void Start()
        {
            enemyRb = GetComponent<Rigidbody>();
            player = GameObject.FindObjectOfType<PlayerController>();
        }

        // Update is called once per frame
        void Update()
        {
            EnemyMove(); 
        }
        void EnemyMove()
        {
            Vector3 lookDirection = (player.transform.position - transform.position).normalized;
            enemyRb.AddForce(lookDirection * speed);
            if (transform.position.y < POSITION_DESTROY_OY)
            {
                Destroy(gameObject);
            }
        }
        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Projectile"))
            {
                Destroy(other.gameObject);
                Destroy(gameObject);
            }
        }
    }
}
