﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Unit4GameplayMechanics
{
    public class EnemyX : MonoBehaviour
    {
        public float speed;
        private Rigidbody enemyRb;
        private GameObject playerGoal;

        public void SetSpeed(float speed)
        {
            this.speed = speed;
        }
        // Start is called before the first frame update
        void Start()
        {
            enemyRb = GetComponent<Rigidbody>();
            playerGoal = GameObject.FindGameObjectWithTag("PlayerGoal");
        }

        // Update is called once per frame
        void Update()
        {
            // Set enemy direction towards player goal and move there
            Vector3 lookDirection = (playerGoal.transform.position - transform.position).normalized;
            enemyRb.AddForce(lookDirection * speed * Time.deltaTime);

        }

        private void OnCollisionEnter(Collision other)
        {
            // If enemy collides with either goal, destroy it
            if (other.gameObject.tag == "Goal" || other.gameObject.tag == "PlayerGoal")
            {
                Destroy(gameObject);
            }
        }

        public void SetForceEnemy(Vector3 dirMove)
        {
            enemyRb.AddForce(dirMove, ForceMode.Impulse);
        }
    }
}
