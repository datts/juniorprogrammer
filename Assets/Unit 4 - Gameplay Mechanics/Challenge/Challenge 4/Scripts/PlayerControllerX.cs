﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Unit4GameplayMechanics
{
    public class PlayerControllerX : MonoBehaviour
    {
        private const int NORMAL_POWER = 10;
        private const int UP_POWER = 25;
        private Rigidbody playerRb;
        private float speed = 500;
        [SerializeField] private RotateCameraX focalPoint;

        public bool hasPowerup;
        public GameObject powerupIndicator;
        public int powerUpDuration = 5;

        [SerializeField] private float powerupStrength = 25; // how hard to hit enemy with powerup

        [SerializeField] private ParticleSystem boostFX;
        void Start()
        {
            playerRb = GetComponent<Rigidbody>();
        }

        void Update()
        {
            PlayerMove();
            SpeedBoost();
            ResetPosition();
        }
        void PlayerMove()
        {
            // Add force to player in direction of the focal point (and camera)
            float verticalInput = Input.GetAxis("Vertical");
            playerRb.AddForce(focalPoint.transform.forward * verticalInput * speed * Time.deltaTime);

            // Set powerup indicator position to beneath player
            powerupIndicator.transform.eulerAngles = Vector3.zero;
        }

        // If Player collides with powerup, activate powerup
        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag("Powerup"))
            {
                Destroy(other.gameObject);
                powerupStrength = UP_POWER;
                StartCoroutine(PowerupCooldown());
                powerupIndicator.SetActive(true);
            }
        }
        // Coroutine to count down powerup duration
        IEnumerator PowerupCooldown()
        {
            yield return new WaitForSeconds(powerUpDuration);
            powerupStrength = NORMAL_POWER;
            powerupIndicator.SetActive(false);
        }
        // If Player collides with enemy
        private void OnCollisionEnter(Collision other)
        {
            if (other.gameObject.CompareTag("Enemy"))
            {
                Vector3 awayFromPlayer = -transform.position + other.gameObject.transform.position;
                var enemy = other.gameObject.GetComponent<EnemyX>();
                if (enemy != null)
                {
                    enemy.SetForceEnemy(awayFromPlayer * powerupStrength);
                }
            }
        }
        private void SpeedBoost()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                playerRb.AddForce(focalPoint.transform.forward * 500 * speed * Time.deltaTime);
                boostFX.Play();
            }
        }
        private void ResetPosition()
        {
            if(transform.position.x > 22 || transform.position.x < -22)
            {
                transform.position = new Vector3(0, 0, 0);
            }
            if (transform.position.z> 35 || transform.position.z < -10)
            {
                transform.position = new Vector3(0, 0, 0);
            }
        }
    }
}
