using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A special building that hold a static reference so it can be found by other script easily (e.g. for Unit to go back
/// to it)
/// </summary>
public class Base : Building
{
    public int id;
    public List<GameObject> resourceBring;
    public GameObject resource;
    public GameObject resourceBag;
    Vector3 position = Vector3.zero;
    public void AddResourceToBag(int resourceNumber)
    {
        for (int i = 0; i < resourceNumber; i++)
        {
            GameObject newResource = Instantiate(resource, resourceBag.transform.position, gameObject.transform.rotation, resourceBag.transform);
            newResource.transform.position += position;
            position += new Vector3(0, 0.5f, 0);
            resourceBring.Add(newResource);
        }
    }
}
