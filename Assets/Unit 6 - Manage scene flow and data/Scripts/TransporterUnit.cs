
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Subclass of Unit that will transport resource from a Resource Pile back to Base.
/// </summary>
public class TransporterUnit : Unit
{
    public int MaxAmountTransported = 1;
    private Building m_CurrentTransportTarget;
    private Building.InventoryEntry m_Transporting = new Building.InventoryEntry();

    public List<GameObject> resourceBring;
    public GameObject resource;
    public GameObject resourceBag;

    [SerializeField]
    private Base[] baseBuild;
    protected override void Start()
    {
        base.Start();
        baseBuild = FindObjectsOfType<Base>();
    }
    public void SetMaxAmountTransported(int amount)
    {
        this.MaxAmountTransported = amount;
    }
    // We override the GoTo function to remove the current transport target, as any go to order will cancel the transport
    public override void GoTo(Vector3 position)
    {
        base.GoTo(position);
        m_CurrentTransportTarget = null;
    }

    protected override void BuildingInRange()
    {
        Base newBase = GetBaseById(id);
        if (m_Target == newBase)
        {
            if (newBase.resourceBring.Count < newBase.InventorySpace)
            {
                //we arrive at the base, unload!
                if (m_Transporting.Count > 0)
                    m_Target.AddItem(m_Transporting.ResourceId, m_Transporting.Count);

                //we go back to the building we came from
                GoTo(m_CurrentTransportTarget);
                newBase.AddResourceToBag(m_Transporting.Count);
                m_Transporting.Count = 0;
                m_Transporting.ResourceId = "";
                DestroyAfterArrive();
            }
            else
            {
                GoTo(newBase.transform.position - new Vector3(1, 0, 1)); // Comback Position next to DropPoint when Droppoint is full
            }
        }
        else
        {
            if (m_Target.Inventory.Count > 0)
            {
                m_Transporting.ResourceId = m_Target.Inventory[0].ResourceId;
                m_Transporting.Count = m_Target.GetItem(m_Transporting.ResourceId, MaxAmountTransported);

                GetListResourceBring(m_Transporting.Count);
                m_CurrentTransportTarget = m_Target;
                GoTo(newBase);
            }
        }
    }

    //Override all the UI function to give a new name and display what it is currently transporting
    public override string GetName()
    {
        return "Transporter";
    }

    public override string GetData()
    {
        return $"Can transport up to {MaxAmountTransported}";
    }

    public override void GetContent(ref List<Building.InventoryEntry> content)
    {
        if (m_Transporting.Count > 0)
            content.Add(m_Transporting);
    }
    public Base GetBaseById(int id)
    {
        for (int i = 0; i <= baseBuild.Length; i++)
        {
            if (baseBuild[i].id == id)
            {
                return baseBuild[i];
            }
        }
        return null;
    }
    public void GetListResourceBring(int resourceNumber)
    {
        Vector3 position = Vector3.zero;
        for (int i = 0; i < resourceNumber; i++)
        {
            GameObject newResource = Instantiate(resource, resourceBag.transform.position, gameObject.transform.rotation, resourceBag.transform);
            newResource.transform.position += position;
            position += new Vector3(0, 0.5f, 0);
            resourceBring.Add(newResource);
        }
    }
    public void DestroyAfterArrive()
    {
        for (int i = 0; i < resourceBring.Count; i++)
        {
            Destroy(resourceBring[i]);
        }
    }
}
