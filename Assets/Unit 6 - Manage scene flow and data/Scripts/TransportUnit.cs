using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TransportUnit
{
    public int id;
    public Color TeamColor;
    public int Speed;
    public int MaxAmountTransported;
}
[System.Serializable]
public class TransportUnitList
{
    public List<TransportUnit> transportUnitList;
}
