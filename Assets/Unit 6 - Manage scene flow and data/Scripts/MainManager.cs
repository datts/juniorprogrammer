using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class MainManager : MonoBehaviour
{
    // Start() and Update() methods deleted - we don't need them right now

    public static MainManager Instance { get; private set; }

    public Color TeamColor; // new variable declared
    public int speed;
    public int amount;

    private void Awake()
    {
        // start of new code
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }
        // end of new code

        Instance = this;
        DontDestroyOnLoad(gameObject);

        LoadColor();
    }

    [System.Serializable]
    class SaveData
    {
        public Color TeamColor;
    }
    public void SaveColor()
    {
        SaveData data = new SaveData();
        data.TeamColor = TeamColor;

        string json = JsonUtility.ToJson(data);

        File.WriteAllText(Application.dataPath + "/Unit 6 - Manage scene flow and data/JSONFile" + "/saveColor.json", json);
    }
    public void LoadColor()
    {
        string path = Application.dataPath + "/Unit 6 - Manage scene flow and data/JSONFile" + "/saveColor.json";
        Debug.Log(path);
        if (File.Exists(path))
        {
            string json = File.ReadAllText(path);
            SaveData data = JsonUtility.FromJson<SaveData>(json);

            TeamColor = data.TeamColor;
        }
    }

    public void LoadUnit()
    {
        string jsonFile = Application.dataPath + "/Unit 6 - Manage scene flow and data/JSONFile" + "/TranUnit.json";
        if (File.Exists(jsonFile))
        {
            string json = File.ReadAllText(jsonFile);
            Debug.Log(json);
            TransportUnit loadPlayerData = JsonUtility.FromJson<TransportUnit>(json);


            TeamColor = loadPlayerData.TeamColor;
            speed = loadPlayerData.Speed;
            amount = loadPlayerData.MaxAmountTransported;

        }


    }

    public TransportUnitList LoadTransportUnitList;
    public void LoadTranList()
    {
        string path = Application.dataPath + "/Unit 6 - Manage scene flow and data/JSONFile" + "/ListUnitTran.json";
        Debug.Log(path);
        if (File.Exists(path))
        {
            string json = File.ReadAllText(path);
            LoadTransportUnitList = JsonUtility.FromJson<TransportUnitList>(json);

            Debug.Log(LoadTransportUnitList.transportUnitList[0].Speed);

        }
    }
}


