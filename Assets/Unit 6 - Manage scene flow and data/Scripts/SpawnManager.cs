using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class SpawnManager : MonoBehaviour
{
    public TransportUnitList LoadTransportUnitList;

    public TransporterUnit transport;
    public GameObject gameObjectTran;

    private Vector3[] positionList = { new Vector3(9, 0.2f, 0), new Vector3(2, 0.2f, -16), new Vector3(-3, 0.2f, -1), new Vector3(-15, 0.2f, -1) };
    // Start is called before the first frame update
    void Start()
    {
        LoadTranList();

        for (int i = 0; i < LoadTransportUnitList.transportUnitList.Count; i++)
        {
            InstantiateTransport(LoadTransportUnitList.transportUnitList[i], positionList[i]);
        }
    }
    public void LoadTranList()
    {
        //string path = Application.dataPath + "/Unit 6 - Manage scene flow and data/JSONFile" + "/SaveFile.json";
        //if (File.Exists(path))
        //{
        //    string json = File.ReadAllText(path);
        //    LoadTransportUnitList = JsonUtility.FromJson<TransportUnitList>(json);

        //}
        var jsonTextFile = Resources.Load<TextAsset>("Transporter/saveFile");
        LoadTransportUnitList = JsonUtility.FromJson<TransportUnitList>(jsonTextFile.text);

    }

    public void InstantiateTransport(TransportUnit transportUnit, Vector3 positionInMap)
    {
        var trans = Instantiate(transport, Vector3.zero, Quaternion.identity);
        trans.SetId(transportUnit.id);
        trans.SetColor(transportUnit.TeamColor);
        trans.SetMaxAmountTransported(transportUnit.MaxAmountTransported);
        trans.SetSpeed(transportUnit.Speed);
        trans.transform.position = positionInMap;
    }

}
