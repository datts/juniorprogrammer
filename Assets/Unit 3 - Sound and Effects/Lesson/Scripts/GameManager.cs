using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;

namespace Unit3SoundAndEffect
{
    public class GameManager : MonoBehaviour
    {
        public bool isGameActive { get; private set; }
        public TextMeshProUGUI timePlay;
        private int timePlayNumber = 0;
        private PlayerController player;
        [SerializeField] private Button startButton;
        [SerializeField] private GameObject timeRunGameOver;
        private void OnEnable()
        {
            Time.timeScale = 0;
        }
        private void Start()
        {
            player = GameObject.FindObjectOfType<PlayerController>();
            isGameActive = false;

            timeRunGameOver.gameObject.SetActive(false);
        }
        public void StartGame()
        {
            Time.timeScale = 1f;
            startButton.gameObject.SetActive(false);

            isGameActive = true;
            StartCoroutine(timePlayToEndGame());
        }
        IEnumerator timePlayToEndGame()
        {
            while (isGameActive == true && player.gameOver == false)
            {
                timePlayNumber++;
                timePlay.text = "Time: " + timePlayNumber.ToString();
                yield return new WaitForSeconds(1f);
            }
            timeRunGameOver.gameObject.GetComponentInChildren<TextMeshProUGUI>().text = "Time run: " + timePlayNumber.ToString();
            timeRunGameOver.gameObject.SetActive(true);
        }

    }
}
