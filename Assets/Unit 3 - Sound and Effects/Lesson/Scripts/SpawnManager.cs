using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Unit3SoundAndEffect
{
    public class SpawnManager : MonoBehaviour
    {
        [SerializeField] private GameObject obstaclePrefabs;
        private Vector3 spawnPos = new Vector3(25, 0.3f, 0);
        private float repeatRate = 2;
        private PlayerController player;

        private void Start()
        {
            player = GameObject.FindObjectOfType<PlayerController>();
            StartCoroutine(SpawnFollowingTime());
        }
        IEnumerator SpawnFollowingTime()
        {
            while (!player.gameOver)
            {
                yield return new WaitForSeconds(repeatRate);
                Instantiate(obstaclePrefabs, spawnPos, obstaclePrefabs.transform.rotation);
                repeatRate -= 0.1f;
            }
        }
    }
}