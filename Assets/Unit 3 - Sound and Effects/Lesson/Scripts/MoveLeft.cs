using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Unit3SoundAndEffect
{
    public class MoveLeft : MonoBehaviour
    {
        [SerializeField] private float speed = 10;
        [SerializeField] private float leftBound = -15;
        private PlayerController player;
        private void Start()
        {
            player = GameObject.FindObjectOfType<PlayerController>();
            StartCoroutine(IncreaseSpeed());
        }
        // Update is called once per frame
        void Update()
        {
            if (player.gameOver == false)
            {
                transform.Translate(Vector3.left * Time.deltaTime * speed);
            }

            if (transform.position.x < leftBound && gameObject.CompareTag("Obstacles"))
            {
                Destroy(gameObject);
            }

        }

        public void SetSpeed()
        {
            speed += 1f;
        }
        IEnumerator IncreaseSpeed()
        {
            while (!player.gameOver)
            {
                yield return new WaitForSeconds(0.5f);
                SetSpeed();
            }
        }
    }
}