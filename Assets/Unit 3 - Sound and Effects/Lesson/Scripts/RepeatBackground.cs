using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Unit3SoundAndEffect
{
    public class RepeatBackground : MonoBehaviour
    {
        private float speed = 10;
        [SerializeField] private Vector3 startPos;
        [SerializeField] private float repeatWidth = 40f;
        private PlayerController player;
        // Start is called before the first frame update
        void Start()
        {
            player = GameObject.FindObjectOfType<PlayerController>();
            startPos = transform.position;
            repeatWidth = GetComponent<BoxCollider>().size.x / 2;
            StartCoroutine(IncreaseSpeed());
        }

        // Update is called once per frame
        void Update()
        {
            if (transform.position.x < startPos.x - repeatWidth)
            {
                transform.position = startPos;
            }
            MoveLeft();
        }
        void MoveLeft()
        {
            if (player.gameOver == false)
            {
                transform.Translate(Vector3.left * Time.deltaTime * speed);
            }
        }
        public void SpeedUp()
        {
            speed += 1f;
        }
        IEnumerator IncreaseSpeed()
        {
            while (!player.gameOver)
            {
                yield return new WaitForSeconds(0.5f);
                SpeedUp();
            }
        }
    }
}
