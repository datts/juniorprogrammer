using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Unit3SoundAndEffect
{
    public class PlayerController : MonoBehaviour
    {
        private Rigidbody playerRb;
        private Animator playerAnim;
        [SerializeField] private ParticleSystem explosionParticle;
        [SerializeField] private ParticleSystem dirtParticle;

        [SerializeField] private AudioClip jumpSound;
        [SerializeField] private AudioClip crashSound;
        private AudioSource playerAudio; 

        [SerializeField] private float jumpForce;
        [SerializeField] private float gravityModifier;
        public bool gameOver { get; private set; }

        [SerializeField] public int maxJumpCount = 2;
        [SerializeField] public int jumpsRemaining = 0;

        // Start is called before the first frame update
        void Start()
        {
            playerRb = GetComponent<Rigidbody>();
            Physics.gravity *= gravityModifier;
            playerAnim = GetComponent<Animator>();
            playerAudio = GetComponent<AudioSource>();
        }

        // Update is called once per frame
        void Update()
        {
            PlayerMove();
        }
        void PlayerMove()
        {
            if (Input.GetKeyDown(KeyCode.Space) && !gameOver && jumpsRemaining > 0)
            {
                playerRb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
                playerRb.velocity = Vector3.zero;
                playerAnim.SetTrigger("Jump_trig");
                dirtParticle.Stop();
                playerAudio.PlayOneShot(jumpSound, 1.0f);
                jumpsRemaining -= 1; // remove 1 jump
            }
        }
        private void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.CompareTag("Ground"))
            {
                dirtParticle.Play();
                jumpsRemaining = maxJumpCount;
            }
            else if (collision.gameObject.CompareTag("Obstacles"))
            {
                gameOver = true;
                Debug.Log("Game Over!");
                playerAnim.SetBool("Death_b", true);
                playerAnim.SetInteger("DeathType_int", 1);
                //Particle
                explosionParticle.Play();
                dirtParticle.Stop();
                //Sound
                playerAudio.PlayOneShot(crashSound, 1.0f);
            }



        }
    }
}