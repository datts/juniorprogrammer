using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Unit3SoundAndEffect
{
    public class Obstacles : MonoBehaviour
    {
        [SerializeField] private float speed = 10;
        [SerializeField] private float leftBound = -15;
        private PlayerController player;
        // Start is called before the first frame update
        private void Start()
        {
            player = GameObject.FindObjectOfType<PlayerController>();
            StartCoroutine(IncreaseSpeed());
        }

        // Update is called once per frame
        void Update()
        {
            MoveLeft();
        }
        void MoveLeft()
        {
            if (player.gameOver == false)
            {
                transform.Translate(Vector3.left * Time.deltaTime * speed);
            }

            if (transform.position.x < leftBound && gameObject.CompareTag("Obstacles"))
            {
                Destroy(gameObject);
            }
        }
        public void SpeedUp()
        {
            speed += 1f;
        }
        IEnumerator IncreaseSpeed()
        {
            while (!player.gameOver)
            {
                yield return new WaitForSeconds(0.5f);
                SpeedUp();
            }
        }
    }
}
