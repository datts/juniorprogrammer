using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Unit3SoundAndEffect
{
    public class SpinObjectX : MonoBehaviour
    {
        public float spinSpeed = -250;
        void Update()
        {
            SpinObject();
        }
        void SpinObject()
        {
            transform.Rotate(Vector3.up, spinSpeed * Time.deltaTime);
        }
    }
}
