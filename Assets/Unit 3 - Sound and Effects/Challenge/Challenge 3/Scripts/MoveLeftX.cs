using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Unit3SoundAndEffect
{
    public class MoveLeftX : MonoBehaviour
    {
        private float speed = 8;
        private PlayerControllerX player;
        private float leftBound = -50;
        // Start is called before the first frame update
        void Start()
        {
            player = GameObject.FindObjectOfType<PlayerControllerX>();
        }

        // Update is called once per frame
        void Update()
        {
            MoveLeft();
        }
        void MoveLeft()
        {
            // If game is not over, move to the left
            if (!player.gameOver)
            {
                transform.Translate(Vector3.left * speed * Time.deltaTime, Space.World);
            }

            // If object goes off screen that is NOT the background, destroy it
            if (transform.position.x < leftBound)
            {
                Destroy(gameObject);
            }
        }
    }
}
