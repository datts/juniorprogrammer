using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace Unit3SoundAndEffect
{
    public class HeartPanel : MonoBehaviour
    {
        // Start is called before the first frame update
        public GameObject[] hearthList;
        public GameObject heart;

        public void SpawnHeart(int timePlay)
        {
            for (int i = 0; i < timePlay; i++)
            {
                GameObject myHearth = (GameObject)Instantiate(heart, transform.position + (new Vector3(-90 + i * 40, 0, 0)), Quaternion.identity);
                myHearth.transform.parent = transform;
            }
            hearthList = GameObject.FindGameObjectsWithTag("Hearth"); //Get Heart to List to easy Destroy
        }
        public void DestroyAllHeath()
        {
            for (int i = 0; i < hearthList.Length; i++)
            {
                Destroy(hearthList[i]);
            }
        }
    }
}
