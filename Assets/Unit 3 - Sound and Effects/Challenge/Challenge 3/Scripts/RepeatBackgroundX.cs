﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Unit3SoundAndEffect
{
    public class RepeatBackgroundX : MonoBehaviour
    {
        private Vector3 startPos;
        public float speed;
        private PlayerControllerX player;

        [SerializeField] private float repeatWidth;

        private void Start()
        {
            startPos = transform.position; // Establish the default starting position 
            repeatWidth = GetComponent<BoxCollider>().size.x / 2; // Set repeat width to half of the background
            player = GameObject.FindObjectOfType<PlayerControllerX>();
        }

        private void Update()
        {
            // If background moves left by its repeat width, move it back to start position
            if (transform.position.x < startPos.x - repeatWidth)
            {
                transform.position = startPos;
            }
            MoveLeft();
        }
        void MoveLeft()
        {
            // If game is not over, move to the left
            if (!player.gameOver)
            {
                transform.Translate(Vector3.left * speed * Time.deltaTime, Space.World);
            }
        }
    }



}


