using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace Unit3SoundAndEffect
{
    public class GameManagerX : MonoBehaviour
    {
        private int point;
        private int pointValue = 5;
        private int health = 3;

        public TextMeshProUGUI pointText;
        [SerializeField] private List<GameObject> hearthPlayer;
        [SerializeField] private HeartPanel heartPanel;
        [SerializeField] private GameObject gameOver;
        [SerializeField] private TextMeshProUGUI highScoreUI;
        [SerializeField] private TextMeshProUGUI yourScoreUI;

        private void Start()
        {
            heartPanel.SpawnHeart(health);
        }
        public void UpdateScore()
        {
            point += pointValue;
            pointText.text = "Point: " + point;
            if (PlayerPrefs.GetInt("HighScore") < point)
            {
                PlayerPrefs.SetInt("HighScore", point);
            }

        }
        public void GameOver()
        {
            gameOver.gameObject.SetActive(true);
            highScoreUI.text = "High Score: " + PlayerPrefs.GetInt("HighScore");
            yourScoreUI.text = "Your Score: " + point;
        }

        public void LostHeart()
        {
            health--;
            heartPanel.DestroyAllHeath();
            heartPanel.SpawnHeart(health);

        }
        public int GetHealth()
        {
            return health;
        }


    }
}
