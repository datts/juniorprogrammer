﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Unit3SoundAndEffect
{
    public class PlayerControllerX : MonoBehaviour
    {
        public bool gameOver;

        private float floatForce = 200f;
        private float gravityModifier = 1.5f;
        [SerializeField] private float heightLimit = 14f;


        [SerializeField] private Rigidbody playerRb;

        public ParticleSystem explosionParticle;
        public ParticleSystem fireworksParticle;

        private AudioSource playerAudio;
        public AudioClip moneySound;
        public AudioClip explodeSound;
        public AudioClip bounceSound;

        private GameManagerX gameManager;

        // Start is called before the first frame update
        void Start()
        {
            Physics.gravity *= gravityModifier;
            playerAudio = GetComponent<AudioSource>();
            playerRb = GetComponent<Rigidbody>();
            // Apply a small upward force at the start of the game
            playerRb.AddForce(Vector3.up * 5, ForceMode.Impulse);

            gameManager = GameObject.FindObjectOfType<GameManagerX>();

        }

        // Update is called once per frame
        void Update()
        {
            // While space is pressed and player is low enough, float up
            if (Input.GetKeyDown(KeyCode.Space) && !gameOver && transform.position.y <= heightLimit)
            {
                playerRb.AddForce(Vector3.up * floatForce, ForceMode.Impulse);
                playerRb.velocity = Vector3.zero;
            }
        }

        private void OnCollisionEnter(Collision other)
        {
            // if player collides with bomb, explode and set gameOver to true
            if (other.gameObject.CompareTag("Bomb"))
            {
                explosionParticle.Play();
                playerAudio.PlayOneShot(explodeSound, 1.0f);
                Destroy(other.gameObject);
                gameManager.LostHeart();
                if (gameManager.GetHealth() == 0)
                {
                    gameOver = true;
                    gameManager.GameOver();
                }
            }

            // if player collides with money, fireworks
            else if (other.gameObject.CompareTag("Money"))
            {
                fireworksParticle.Play();
                playerAudio.PlayOneShot(moneySound, 1.0f);
                Destroy(other.gameObject);
                gameManager.UpdateScore();

            }
            else if (other.gameObject.CompareTag("Ground") && !gameOver)
            {
                fireworksParticle.Play();
                playerAudio.PlayOneShot(moneySound, 1.0f);
                playerRb.AddForce(Vector3.up * floatForce, ForceMode.Impulse);
            }

        }
        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag("Sky") && !gameOver)
            {
                fireworksParticle.Play();
                playerAudio.PlayOneShot(moneySound, 1.0f);
                playerRb.AddForce(Vector3.down * floatForce, ForceMode.Impulse);
            }
        }

    }

}
